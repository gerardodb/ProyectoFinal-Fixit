package com.proyecto.fixit.model;

import java.sql.Timestamp;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Cascade;
import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;

@Entity
@Table(name="Mensaje")
public class Mensaje {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	@NotNull
	@ManyToOne
	//@JsonIgnore	
	Especialista especialista;
	
	@NotNull
	@ManyToOne
	//@JsonIgnore
	Cliente cliente;
	
	@NotNull
	char remitente; //Una C significa que el remitente es el cliente, Una E significa que el remitente es el especialista

	@NotNull
	String asunto;
	
	@NotNull
	String cuerpo;
	
	@DateTimeFormat(pattern="yyyy-MM-dd'T'hh:mm")
	Timestamp FechaHora;
	
	//@JsonIgnore
	boolean urgencia =false;
	
	@ManyToOne(optional=true, cascade= CascadeType.ALL)
	//@JsonIdentityReference(alwaysAsId=true)
	Servicio servicio;
	
	@ManyToOne(optional=true)
	//@JsonIdentityReference(alwaysAsId=true)
	ServicioUrgente servicioUrgente;
	
	@ManyToOne(optional=true)
	Publicacion publicacion;
	
	public Publicacion getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}

	public ServicioUrgente getServicioUrgente() {
		return servicioUrgente;
	}

	public void setServicioUrgente(ServicioUrgente servicioUrgente) {
		this.servicioUrgente = servicioUrgente;
	}

	public Servicio getServicio() {
		return servicio;
	}

	public void setServicio(Servicio servicio) {
		this.servicio = servicio;
	}

	public boolean isUrgencia() {
		return urgencia;
	}

	public void setUrgencia(boolean urgencia) {
		this.urgencia = urgencia;
	}

	public String getAsunto() {
		return asunto;
	}

	public void setAsunto(String asunto) {
		this.asunto = asunto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Especialista getEspecialista() {
		return especialista;
	}

	public void setEspecialista(Especialista especialista) {
		this.especialista = especialista;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public char getRemitente() {
		return remitente;
	}

	public void setRemitente(char remitente) {
		this.remitente = remitente;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	public Timestamp getFechaHora() {
		return FechaHora;
	}

	public void setFechaHora(Timestamp fechaHora) {
		FechaHora = fechaHora;
	}
}
