package com.proyecto.fixit.model;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity(name="Servicio")
public class Servicio {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	int id;
	
	double precio;
	
	@NotNull
	@DateTimeFormat(pattern="yyyy-MM-dd'T'hh:mm")
	Timestamp fechaHora;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name="publicacion_id")
	Publicacion publicacion;
	
	@ManyToOne
	@JoinColumn(name="cliente_id")
	@JsonBackReference
	Cliente cliente;
	
	double calificacion;
	
	String comentario;
	
	public String getComentario() {
		return comentario;
	}

	public void setComentario(String comentario) {
		this.comentario = comentario;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Timestamp getFechaHora() {
		return fechaHora;
	}
	
	public String getFechaHoraFormateada() {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm");
		return format.format(fechaHora.getTime());
	}

	public void setFechaHora(Timestamp fechaHora) {
		this.fechaHora = fechaHora;
	}

	public Publicacion getPublicacion() {
		return publicacion;
	}

	public void setPublicacion(Publicacion publicacion) {
		this.publicacion = publicacion;
	}
	
	
}
