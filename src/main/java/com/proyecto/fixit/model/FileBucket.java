package com.proyecto.fixit.model;

import org.springframework.web.multipart.MultipartFile;

public class FileBucket {
	String nombre;
	
    MultipartFile file;
     
    public MultipartFile getFile() {
        return file;
    }
 
    public void setFile(MultipartFile file) {
        this.file = file;
    }

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
    
}