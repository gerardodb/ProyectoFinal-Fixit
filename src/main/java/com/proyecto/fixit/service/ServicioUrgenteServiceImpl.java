package com.proyecto.fixit.service;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.ServicioUrgenteDao;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

@Transactional
@Service("servicioUrgenteService")
public class ServicioUrgenteServiceImpl implements ServicioUrgenteService{
	@Autowired
	ServicioUrgenteDao dao;
	@Override
	public List<ServicioUrgente> findAllMenores24Hs() {
		List<ServicioUrgente> listMenoresA24Hs = new ArrayList<ServicioUrgente>();
		Timestamp actual = new Timestamp(System.currentTimeMillis());
		
		for (ServicioUrgente servicioUrgente : dao.findAll()) {
			Timestamp dateTime= servicioUrgente.getDateTime();
			Timestamp tiempoLimiteParaRespuesta= servicioUrgente.getTiempoLimiteParaRespuesta();
			long diferenciaMinutos = tiempoLimiteParaRespuesta.getTime()- actual.getTime();
			if ( ( (actual.getTime()- dateTime.getTime())/1000/60/60 < 24) && diferenciaMinutos > 0){
				listMenoresA24Hs.add(servicioUrgente);
			}
		}
		return listMenoresA24Hs;
	}
	@Override
	public void saveServicioUrgente(ServicioUrgente servicioUrgente) throws Exception {
		dao.saveServicioUrgente(servicioUrgente);
	}
	@Override
	public List<ServicioUrgente> getServiciosUrgentesMenoresDistancia(float latActual, float lngActual, int idEspecialista) {
		System.out.println("La latidud es " + latActual + " y la longitud es "+ lngActual);
		List<ServicioUrgente> listMenoresADistancia = new ArrayList<ServicioUrgente>();
		for (ServicioUrgente servicioUrgente : dao.findAll()) {
			System.out.println("La distancia es: " + getDistance(servicioUrgente.getLatitud(), servicioUrgente.getLongitud(), latActual, lngActual));
			System.out.println("La distancia max es: "+(servicioUrgente.getDistanciaMax()*1000));
			if (getDistance(servicioUrgente.getLatitud(), servicioUrgente.getLongitud(), latActual, lngActual)
					<=(servicioUrgente.getDistanciaMax()*1000)
					&& (servicioUrgente.getTiempoLimiteParaRespuesta().getTime()>= System.currentTimeMillis())
					&& (servicioUrgente.getEspecialista()==null || servicioUrgente.getEspecialista().getId()==idEspecialista)){ //Se multiplica por mil para convertir los kilometros en metros
				listMenoresADistancia.add(servicioUrgente);
			}
		}
		return listMenoresADistancia;
	}
	
	private double radio(float x) {
		  return x * Math.PI / 180;
	};

	private double getDistance (float p1Lat, float p1Lng, float p2Lat, float p2Lng) {
	  int R = 6378137; // Earth’s mean radius in meter
	  double dLat = radio(p2Lat - p1Lat);
		  double dLong = radio(p2Lng - p1Lng);
		  double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		    Math.cos(radio(p1Lat)) * Math.cos(radio(p2Lat)) *
		    Math.sin(dLong / 2) * Math.sin(dLong / 2);
		  double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		  double d = R * c;
		  return d; // returns the distance in meter
		}

	@Override
	public ServicioUrgente getById(int id) {
		return dao.getById(id);
	}
	@Override
	public List<ServicioUrgente> findAllByUser(User user) {
		return dao.findAllByUser(user);
		
	}
	@Override
	public void update(ServicioUrgente servicioUrgente) {
		dao.update(servicioUrgente);
	}
	@Override
	public void delete(ServicioUrgente servicioUrgente) {
		dao.delete(servicioUrgente);
	};

}
