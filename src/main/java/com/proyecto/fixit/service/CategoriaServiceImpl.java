package com.proyecto.fixit.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.CategoriaDao;
import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.User;

@Service("categoriaService")
@Transactional
public class CategoriaServiceImpl implements CategoriaService {
	@Autowired
	CategoriaDao dao;

	@Override
	public List<Categoria> findAllCategorias() {
		return dao.findAllCategoria();
	}

//	@Override
//	public List<Categoria> findAllCategoriasPadres() {
//		List<Categoria> listCategorias =  findAllCategorias();
//		List<Categoria> listCategoriasPadres =  new ArrayList<Categoria>();
//		for (int i=0; i< listCategorias.size(); i++) {
//			System.out.println("la categoria es: "+ listCategorias.get(i).getNombre());
//			if (listCategorias.get(i).getSubCategorias().size()>0) {
//				
//				listCategoriasPadres.add(listCategorias.get(i));
//			}
//		}
//		System.out.println("La cantidad de categorias totales son: " + listCategorias.size());
//		System.out.println("La lista de categorias padres son: " + listCategoriasPadres.toString());
//		System.out.println("Las cantidad de subcategorias del primer padre es: " + listCategoriasPadres.get(0).getSubCategorias().size());
//		System.out.println("Las cantidad de subcategorias del segundo padre es: " + listCategoriasPadres.get(1).getSubCategorias().size());
//		return listCategoriasPadres;
//	}
	
	@Override
	public List<Categoria> findAllCategoriasPadres(){
		return dao.findAllCategoriasPadres();
	}
	
//	@Override
//	public List<Categoria> findAllSubcategorias() {
//		List<Categoria> listCategorias =  findAllCategorias();
//		List<Categoria> listCategoriasPadres =  new ArrayList<Categoria>();
//		for (int i=0; i< listCategorias.size(); i++) {
//			System.out.println("la categoria es: "+ listCategorias.get(i).getNombre());
//			if (listCategorias.get(i).getSubCategorias().size()==0) {
//				listCategoriasPadres.add(listCategorias.get(i));
//			}
//		}
//		return listCategoriasPadres;
//	}
	
	@Override
	public List<Categoria> findAllSubcategorias() {
		return dao.findAllSubcategorias();
	}

	@Override
	public Categoria getByKeyCategoria(int key) {
		return dao.getById(key);
	}
	
	@Override
	public Categoria getByNombre(String nombre) {
		return dao.getByNombre	(nombre);
	}
	
	@Override
	public void saveCategoria(Categoria categoria) {
		dao.save(categoria);
	}
	
	@Override
	public void updateCategoria(Categoria categoria) {
		dao.update(categoria);
	}
	
	@Override
	public void updateUpdateCategoria(Categoria categoria) {
		dao.updateUpdate(categoria);
	}

	@Override
	public void deleteCategoria(Categoria categoria) {
		dao.deleteCategoria(categoria);
	}
}
