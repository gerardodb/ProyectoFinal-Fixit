package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Rol;
import com.proyecto.fixit.model.User;

public interface UserService {
	void saveUser(User user);
	User getbyUsername(String username);
	User getById(int id);
	void update(User user);
	List<User> findAll();
}
