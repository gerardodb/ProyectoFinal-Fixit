package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Publicacion;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

public interface MensajeService {
	void save(Mensaje mensaje);
	List<Mensaje> findAll(Especialista especialista, Cliente cliente);
	List<Mensaje> findAllByServicioUrgente(ServicioUrgente servicioUrgente);
	List<Mensaje> findAllByPublicacion(Publicacion publicacion);
	List<Mensaje> findAll(Especialista especialista);
	List<Mensaje> findAll(Cliente cliente);
	List<Mensaje> findAllDeServicio(Cliente cliente);
}
