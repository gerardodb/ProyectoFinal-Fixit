package com.proyecto.fixit.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyecto.fixit.dao.ClienteDao;
import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.User;

@Transactional
@Service("clienteService")
public class ClienteServiceImpl implements ClienteService {
	 @Autowired
	 ClienteDao dao;

	@Override
	public Cliente getByUser(User user) {
		return dao.getByUser(user);
	}

	@Override
	public void updateCliente(Cliente cliente) {
		Cliente entity = dao.findById(cliente.getId());
		entity.getNombre();
		if(entity!=null){
			entity.setNombre(cliente.getNombre());
			entity.setApellido(cliente.getApellido());
			entity.setDireccion(cliente.getDireccion());
			entity.setEmail(cliente.getEmail());
			entity.setServiciosContratados(cliente.getServiciosContratados());
			entity.setTelefono(cliente.getTelefono());
			entity.setUser(cliente.getUser());
			entity.setLatitud(cliente.getLatitud());
			entity.setLongitud(cliente.getLongitud());
			dao.save(entity);
		}
	}

	@Override
	public void saveCliente(Cliente cliente) {
		dao.save(cliente);
	}
}
