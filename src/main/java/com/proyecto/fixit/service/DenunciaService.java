package com.proyecto.fixit.service;

import java.util.List;

import com.proyecto.fixit.model.Denuncia;
import com.proyecto.fixit.model.User;

public interface DenunciaService {
	void save(Denuncia denuncia);
	List<Denuncia> findAll();
}
