package com.proyecto.fixit.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.proyecto.fixit.model.Rol;
import com.proyecto.fixit.service.RolService;
 
/**
 * A converter class used in views to map id's to actual userProfile objects.
 */
@Component
public class RoleToRolConverter implements Converter<Object, Rol>{
 
    @Autowired
    RolService rolService;
 
    /**
     * Gets UserProfile by Id
     * @see org.springframework.core.convert.converter.Converter#convert(java.lang.Object)
     */
    public Rol convert(Object element) {
    	System.out.println("entro al converted!!!!!!!!!!!!!!!!!");
        Integer id = Integer.parseInt((String)element);
        Rol rol= rolService.findById(id);
        System.out.println("Profile : "+rol);
        return rol;
    }
     
}
