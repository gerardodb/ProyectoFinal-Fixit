package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Rol;

public interface RolDao {
	List<Rol> findAll();
	
	Rol findByType(String type);
	
	Rol findById(int id);
}
