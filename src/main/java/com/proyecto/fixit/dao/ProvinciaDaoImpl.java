package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Provincia;

@Repository("provinciaDao")
public class ProvinciaDaoImpl extends AbstractDao<Integer, Provincia> implements ProvinciaDao {

	@Override
	public List<Provincia> findAllCategorias() {
		Criteria criteria= createEntityCriteria();
		List<Provincia> list = (List<Provincia>) criteria.list();
		return list;
	}

	@Override
	public Provincia getById(int id) {
		return this.getByKey(id);
	}

}
