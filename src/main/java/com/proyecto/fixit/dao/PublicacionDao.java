package com.proyecto.fixit.dao;

import java.util.List;
import java.util.Locale.Category;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Publicacion;

public interface PublicacionDao {
	List<Publicacion> findAllPublicacion();
	List<Publicacion> findAllPublicacionesActivas();
	List<Publicacion> findAll8ultimasActivas();
	Publicacion getById(int key);
	List<Publicacion> findAllActivasByEspecialista(Especialista especialista);
	List<Publicacion> getByCriterioBusqueda(String criterioBusqueda);
	List<Publicacion> findAllByCategory(Categoria category, int desde, int hasta);
	List<Publicacion> findAllByCategoryByCiudad(Categoria category, int desde, int hasta, Ciudad ciudad);
	List<Publicacion> findAllByCategoryByCiudad(Categoria category, Ciudad ciudad);
	void deleteById(int id);
	
	void update(Publicacion publicacion);
	
	public Double getPromCalifPub(int idPub);
	
	void savePublicacion (Publicacion publicacion);
}
