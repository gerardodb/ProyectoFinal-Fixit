package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Categoria;
import com.proyecto.fixit.model.User;

@Repository("categoriaDao")
public class CategoriaDaoImp extends AbstractDao<Integer, Categoria> implements CategoriaDao {

	@Override
	public List<Categoria> findAllCategoria() {
		Criteria criteria= createEntityCriteria();
		List<Categoria> list = (List<Categoria>) criteria.list();
		return list;
	}

	@Override
	public Categoria getById(int key) {
		return getByKey(key);
	}
	
	@Override
	public Categoria getByNombre(String nombre) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("nombre", nombre));
		return (Categoria) criteria.uniqueResult();
	}
	
	@Override
	public void save(Categoria categoria) {
		persist(categoria);
	}
	
	@Override
	public void update(Categoria categoria) {
		getSession().merge(categoria);
	}
	
	@Override
	public void updateUpdate(Categoria categoria) {
		getSession().update(categoria);
	}
	
	@Override
	public List<Categoria> findAllCategoriasPadres(){
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.isNull("padre"));
		return criteria.list();
	}

	@Override
	public List<Categoria> findAllSubcategorias() {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.isNotNull("padre"));
		
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		System.out.println("la cantidad devuelta de subcategorias es: "+ criteria.list().size());
		return criteria.list();
	}

	@Override
	public void deleteCategoria(Categoria categoria) {
		getSession().delete(categoria);
	}


}
