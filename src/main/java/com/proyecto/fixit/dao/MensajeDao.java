package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Mensaje;
import com.proyecto.fixit.model.Publicacion;
import com.proyecto.fixit.model.ServicioUrgente;
import com.proyecto.fixit.model.User;

public interface MensajeDao {
	List<Mensaje> findAll(Especialista especialista, Cliente cliente);
	List<Mensaje> findAll(Especialista especialista);
	List<Mensaje> findAll(Cliente cliente);
	List<Mensaje> findAllByServicios(Cliente cliente);
	List<Mensaje> findAllByServicioUrgente(ServicioUrgente servicioUrgente);
	List<Mensaje> findAllByPublicacion(Publicacion publicacion);
	void save(Mensaje mensaje);
}
