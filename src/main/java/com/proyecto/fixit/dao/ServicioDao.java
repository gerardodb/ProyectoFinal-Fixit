package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Servicio;

public interface ServicioDao {
	void updateServicio(Servicio servicio);
	void save(Servicio servicio);
	List<Servicio> getServiciosByEspecialista (Especialista especialista);
	Servicio findByKey(int key);}
