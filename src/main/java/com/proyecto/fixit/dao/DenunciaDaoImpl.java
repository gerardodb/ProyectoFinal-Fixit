package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Denuncia;
import com.proyecto.fixit.model.User;

@Repository("denunciaDao")
public class DenunciaDaoImpl extends AbstractDao<Integer, Denuncia> implements DenunciaDao{

	@Override
	public void save(Denuncia denuncia) {
		getSession().save(denuncia);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Denuncia> findAll() {
		Criteria crit = createEntityCriteria();
        crit.addOrder(Order.asc("fechaHora"));
        return (List<Denuncia>)crit.list();
	}
}
