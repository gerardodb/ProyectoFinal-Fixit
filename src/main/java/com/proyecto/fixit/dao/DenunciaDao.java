package com.proyecto.fixit.dao;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Denuncia;
import com.proyecto.fixit.model.User;

public interface DenunciaDao {
	void save(Denuncia denuncia);
	List<Denuncia> findAll();
}
