package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Ciudad;
import com.proyecto.fixit.model.Provincia;

public interface CiudadDao {
	Ciudad getByKey(int key);
	List<Ciudad> findAll();
	List<Ciudad> findAll(Provincia provincia);
}
