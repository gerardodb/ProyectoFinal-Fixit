package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Rol;

@Repository("rolDao")
public class RolDaoImpl extends AbstractDao<Integer, Rol> implements RolDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Rol> findAll() {
		Criteria crit = createEntityCriteria();
        crit.addOrder(Order.asc("type"));
        return (List<Rol>)crit.list();
	}

	@Override
	public Rol findByType(String type) {
		Criteria crit = createEntityCriteria();
        crit.add(Restrictions.eq("type", type));
        return (Rol) crit.uniqueResult();
	}

	@Override
	public Rol findById(int id) {
		return getByKey(id);
	}

}
