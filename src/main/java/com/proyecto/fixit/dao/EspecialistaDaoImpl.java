package com.proyecto.fixit.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Employee;
import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.User;

@Repository
public class EspecialistaDaoImpl extends AbstractDao<Integer, Especialista> implements EspecialistaDao{

	@Override
	public Especialista getByKey(int key) {
		return super.getByKey(key);
	}

	@Override
	public Especialista getByUsername(User user) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		return (Especialista) criteria.uniqueResult();
	}

	@Override
	public void save(Especialista especialista) {
		getSession().save(especialista);
	}

	@Override
	public void update(Especialista especialista) {
		getSession().update(especialista);
	}
}
