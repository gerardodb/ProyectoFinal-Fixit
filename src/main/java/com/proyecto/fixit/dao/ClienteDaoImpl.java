package com.proyecto.fixit.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Cliente;
import com.proyecto.fixit.model.Employee;
import com.proyecto.fixit.model.User;

@Repository("clienteDao")
public class ClienteDaoImpl extends AbstractDao<Integer, Cliente> implements ClienteDao {

	@Override
	public Cliente findById(int id) {
		return getByKey(id);
	}
	
	@Override
	public Cliente getByUser(User user) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("user", user));
		return (Cliente) criteria.uniqueResult();
	}

	@Override
	public void save(Cliente cliente) {
		persist(cliente);
	}	
}
