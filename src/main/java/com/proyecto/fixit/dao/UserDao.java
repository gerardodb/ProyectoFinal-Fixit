package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Rol;
import com.proyecto.fixit.model.User;

public interface UserDao {
	void save (User user);
	User getByUsername(String username);
	User getById(int id);
	void update(User user);
	List<User> findAll();
}
