package com.proyecto.fixit.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.proyecto.fixit.model.Especialista;
import com.proyecto.fixit.model.Rol;
import com.proyecto.fixit.model.User;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	@Override
	public void save(User user) {
		persist(user);
	}

	@Override
	public User getByUsername(String username) {
		Criteria criteria= createEntityCriteria();
		criteria.add(Restrictions.eq("username", username));
		return (User) criteria.uniqueResult();
	}

	@Override
	public User getById(int id) {
		return this.getByKey(id);
	}
	
	@Override
	public void update(User user) {
		getSession().update(user);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<User> findAll() {
		Criteria crit = createEntityCriteria();
        crit.addOrder(Order.asc("username"));
        return (List<User>)crit.list();
	}
}

