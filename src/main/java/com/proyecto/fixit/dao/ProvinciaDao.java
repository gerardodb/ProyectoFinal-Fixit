package com.proyecto.fixit.dao;

import java.util.List;

import com.proyecto.fixit.model.Provincia;

public interface ProvinciaDao {
	List<Provincia> findAllCategorias();
	Provincia getById(int id);
}
