<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="ISO-8859-1">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<script type="text/javascript">
	window.onload = function() {
		$("#liBloqueos").attr("class", "active");
	}
</script>
</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="index">Home</a></li>
						<li>Bloquear/Desbloquear Usuario</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="customer-orders">
					<div class="box">
						<h1>Lista de usuarios de Fixit</h1>

						<!--                         <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p> -->
						<p class="text-muted">Esta es la lista de todos los usuarios registrados en el sistema.</p>

						<hr>

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Username</th>
										<th>Password</th>
										<th>Email</th>
										<th>Bloqueado?</th>
										<th><i class="fa fa-hand-o-down"></i></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaUsers}" var="user">
										<c:choose>
											<c:when test="${user.enabled == false}">
												<tr bgcolor="#F9EBEA">
											</c:when>
											<c:otherwise>
												<tr bgcolor=#E9F7EF>
											</c:otherwise>
										</c:choose>
										<td>${user.username}</td>
										<td>${user.password}</td>
										<td>${user.mail}</td>
										<c:choose>
											<c:when test="${user.enabled == false}">
												<th>SI</th>
												<td>
													<a href="bloqdesbloquser-${user.id}" title="Click para desbloquear usuario">
														<i class="fa fa-check-circle-o"></i>
													</a>
										        </td>
											</c:when>
											<c:otherwise>
												<th>NO</th>
												<td>
													<a href="bloqdesbloquser-${user.id}" title="Click para bloquear usuario">
														<i class="fa fa-ban"></i>
													</a>
												</td>
											</c:otherwise>
										</c:choose>
									</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***____________________ -->
		<%@include file="footer.jsp"%>

		<div class="modal fade" id="confirm-delete" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">Eliminar urgencia</div>
					<div class="modal-body">¿Está seguro que desea eliminar la
						solicitud de urgencia?</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a href="#" id="buttonEliminarModal" class="btn btn-danger btn-ok">Eliminar</a>
					</div>
				</div>
			</div>
		</div>
</body>

<script type="text/javascript">
	function setValorBotonEliminarModal(obj) {
		$("#buttonEliminarModal").attr("href", obj.getAttribute("value"));
		return false;
	}
</script>

<script type="text/javascript">
	function cambiarIconoaEliminar(obj) {
		$("#icon" + obj.getAttribute("id")).attr("class", "fa fa-times");
		//$("#"+obj.getAttribute("id")).attr("class", "btn btn-primary btn-sm")
	}
	function cambiarIconoaReloj(obj) {
		$("#icon" + obj.getAttribute("id")).attr("class", "fa fa-clock-o");
		//$("#"+obj.getAttribute("id")).attr("class", "btn btn-primary btn-sm")
	}
</script>
</html>

