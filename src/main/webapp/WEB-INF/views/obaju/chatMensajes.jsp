<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">
<link rel="stylesheet" href="css/chat.css">
</head>
<body>
	
			<c:forEach items="${listaChatMensajes}" var="mensaje">
				<c:choose>
					<c:when test="${mensaje.remitente eq 'C'.charAt(0) and rolLogueado == 'Especialista'}">
						<li class="other">
					</c:when>
					<c:when test="${mensaje.remitente eq 'E'.charAt(0)  and rolLogueado == 'Cliente'}">
						<li class="other">
					</c:when>
					<c:otherwise>
						<li class="self">
					</c:otherwise>
				</c:choose>
				<div class="avatar">
					<img src="https://i.imgur.com/DY6gND0.png" draggable="false" />
				</div>
				<div class="msg">
					<p>${mensaje.cuerpo}</p>
					<time>
						<fmt:formatDate value="${mensaje.fechaHora}"
							pattern="dd/MM/yyyy HH:mm" />
					</time>
				</div>
			</c:forEach>

</body>

</html>

