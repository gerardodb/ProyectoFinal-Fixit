<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<meta name="_csrf" content="${_csrf.token}" />
<!-- default header name is X-CSRF-TOKEN -->
<meta name="_csrf_header" content="${_csrf.headerName}" />

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- Estilo para la calificacion -->
<link href="css/star-rating.css" media="all" rel="stylesheet"
	type="text/css" />
<!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
<link
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
	rel="stylesheet">


<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<!-- Estilo para la calificacion -->
<link rel="stylesheet" href="css/starrr.css">
<script type="text/javascript">
	window.onload = function() {
		$("#liCategorias").attr("class", "active");
	}
</script>

</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="index">Home</a></li>
						<li>Administracion categorias</li>
					</ul>

				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="customer-orders">
					<div class="box">



						<a href="crearcategoria-0" style="float: right;"
							class="btn btn-success glyphicon glyphicon-plus"> Agregar
							nueva categoria</a>
						<h1>Categorias padres</h1>
						<hr>
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Accion</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaPadres}" var="categoriaPadre">
										<tr>
											<td>${categoriaPadre.nombre}</td>
											<td><a href="crearcategoria-${categoriaPadre.id}"><button
														class="btn btn-info btn-xs" data-title="Editar">
														<span class="glyphicon glyphicon-pencil"></span>
													</button> </a> <a
												onclick="setValorBotonEliminarCategoriaModal(${categoriaPadre.id}); return false;"
												value="delete-categoria-${categoriaPadre.id}.html"
												data-toggle="modal" data-target="#confirm-delete"><button
														class="btn btn-danger btn-xs" data-title="Eliminar">
														<span class="glyphicon glyphicon-trash"></span>
													</button> </a></td>

										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>

						<h1>Categorias hijas</h1>
						<hr>

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										
										<th>Nombre</th>
										<th>Nombre padre</th>
										<th>Accion</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaSubcategorias}" var="subcategoria">
										<tr>
											<td>${subcategoria.nombre}</td>
											<td>${subcategoria.padre.nombre}</td>
											<td><a href="crearcategoria-${subcategoria.id}"><button
														class="btn btn-info btn-xs" data-title="Editar">
														<span class="glyphicon glyphicon-pencil"></span>
													</button> </a> <a
												onclick="setValorBotonEliminarCategoriaModal(${subcategoria.id}); return false;"
												value="delete-categoria-${subcategoria.id}.html"
												data-toggle="modal" data-target="#confirm-delete"><button
														class="btn btn-danger btn-xs" data-title="Eliminar">
														<span class="glyphicon glyphicon-trash"></span>
													</button> </a></td>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<div class="modal fade" id="modalComentario" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">Registrar comentario</div>

					<div class="modal-body">
						<textarea required="required" pattern="[A-Za-z0-9]{1,20}"
							id="textAreaComentario" class="form-control" maxlength="250"
							rows="3"
							placeholder="Ingrese un comentario (no mas de 250 caracteres) sobre su experiencia con el servicio brindado"></textarea>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a href="#" onclick="ajaxComentarServicio()" name=""
							id="buttonEliminarModal" class="btn btn-danger btn-ok">
							Guardar</a>
					</div>
				</div>
			</div>
		</div>



		<!-- *** FOOTER ***
 _________________________________________________________ -->
		<%@include file="footer.jsp"%>

		<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
		<div class="modal fade" id="confirm-delete" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">Eliminar categoria</div>
					<div class="modal-body">�Esta seguro que desea eliminar la
						categoria?</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a href="#" id="buttonEliminarCategoriaModal"
							class="btn btn-danger btn-ok">Eliminar</a>
					</div>
				</div>
			</div>
		</div>




		<script src="js/star-rating.js" type="text/javascript"></script>

		<script>
			function setValorBotonEliminarCategoriaModal(obj) {
				$("#buttonEliminarCategoriaModal").attr("href", "eliminarcategoria-"+obj);
				//return false;
			}

			function setCalificacion(obj) {
				$('#' + obj.getAttribute('id'))
						.on(
								'rating:change',
								function(event, value, caption) {

									console.log(value);
									console.log(caption);
									var idServicio = obj.getAttribute("name");

									var token = $("meta[name='_csrf']").attr(
											"content");
									var header = $("meta[name='_csrf_header']")
											.attr("content");

									var datos = {
										id : idServicio,
										calificacion : value
									};

									$.ajax({
										type : "POST",
										contentType : "application/json",
										url : 'calificar-servicio2',
										data : JSON.stringify(datos),
										dataType : 'json',
										timeout : 100000,
										beforeSend : function(request) {
											request.setRequestHeader(header,
													token);
										},
										success : function(datos) {
											console.log("SUCCESS: ", datos);

										},
										error : function(e) {
											console.log("ERROR: ", e);
											display(e);
										},
										done : function(e) {
											console.log("DONE");
										}
									});
								});
			}

			function ajaxComentarServicio() {
				var count = $("#textAreaComentario").val().length;
				if (count == 0) {
					alert("Complete el campo");
					return false;
				}

				var token = $("meta[name='_csrf']").attr("content");
				var header = $("meta[name='_csrf_header']").attr("content");
				var text = document.getElementById("textAreaComentario").value;
				var idServicio = $('#buttonEliminarModal').attr("name");
				var datos = {
					id : idServicio,
					comentario : text
				};
				$.ajax({
					type : "POST",
					contentType : "application/json",
					url : 'comentar-servicio',
					data : JSON.stringify(datos),
					dataType : 'json',
					timeout : 100000,
					beforeSend : function(request) {
						request.setRequestHeader(header, token);
					},
					success : function(datos) {
						console.log("SUCCESS: ", datos);
						$('#modalComentario').modal('hide');
						alert("El comentario se registro correctamente");
					},
					error : function(e) {
						console.log("ERROR: ", e);
					},
					done : function(e) {
						console.log("DONE");
					}
				});
			}
		</script>

		<script>
			function showModalComentario(idPublicacion) {
				$("#buttonEliminarModal").attr("name", idPublicacion);
				$('#modalComentario').modal('toggle');
			}
		</script>
</body>
</html>

