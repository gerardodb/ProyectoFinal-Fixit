<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">

<head>

<!-- Latest compiled and minified CSS -->


<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/css/bootstrap-select.min.css">

</head>

<body>

	<div class="col-md-12">
		<ul class="breadcrumb">
			<li><a href="index">Inicio</a></li>
			<li>${subcategoriaSeleccionada.nombre}</li>
		</ul>
	</div>

	<div class="col-md-3">
		<!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
		<!-- 		<div class="panel panel-default sidebar-menu"> -->

		<!-- 			<div class="panel-heading"> -->
		<!-- 				<h3 class="panel-title">Categorias</h3> -->
		<!-- 			</div> -->

		<!-- 			<div class="panel-body"> -->
		<!-- 				<ul class="nav nav-pills nav-stacked category-menu"> -->
		<!-- 													<li><a href="category.html">Men <span -->
		<!-- 															class="badge pull-right">42</span></a> -->
		<!-- 														<ul> -->
		<!-- 															<li><a href="category.html">T-shirts</a></li> -->
		<!-- 															<li><a href="category.html">Shirts</a></li> -->
		<!-- 															<li><a href="category.html">Pants</a></li> -->
		<!-- 															<li><a href="category.html">Accessories</a></li> -->
		<!-- 														</ul></li> -->



		<%-- 					<c:forEach items="${categoriasPadres}" var="categoriaPadre"> --%>
		<%-- 						<c:choose> --%>
		<%-- 							<c:when --%>
		<%-- 								test="${categoriaPadre.id == subcategoriaSeleccionada.id}"> --%>
		<!-- 								<li class="active"><a -->
		<%-- 									href="category-${categoriaPadre.id}-0-12">${subcategoriaSeleccionada.nombre} --%>
		<!-- <!-- 										<span class="badge pull-right">123</span> -->
		
		<!-- 								</a></li> -->
		<%-- 							</c:when> --%>
		<%-- 							<c:otherwise> --%>
		<%-- 								<li><a href="category-${categoriaPadre.id}-0-12">${categoriaPadre.nombre} --%>
		<!-- <!-- 										<span class="badge pull-right">42</span> -->
		
		<!-- 								</a></li> -->
		<%-- 							</c:otherwise> --%>
		<%-- 						</c:choose> --%>
		<!-- 						<ul> -->
		<%-- 							<c:forEach items="${categoriaPadre.subCategorias}" --%>
		<%-- 								var="subcategoria"> --%>
		<%-- 								<li><a href="category-${subcategoria.id }-0-12">${subcategoria.nombre }</a> --%>
		<!-- 								</li> -->
		<%-- 							</c:forEach> --%>
		<!-- 						</ul> -->
		<!-- 						</li> -->
		<%-- 					</c:forEach> --%>
		<!-- 				</ul> -->

		<!-- 			</div> -->
		<!-- 		</div> -->

		<div class="panel panel-default sidebar-menu">
			<div class="panel-heading">
				<h3 class="panel-title">
					Busqueda en ${ciudad.nombre}
					<!-- 					<a class="btn btn-xs btn-danger pull-right" href="#"><i -->
					<!-- 						class="fa fa-times-circle"></i> Limpiar</a> -->
				</h3>
			</div>

			<div class="panel-body">
				<select id="selectOptionProvincia" class="selectpicker" title="Provincia"
					data-live-search="true" onchange="setearItemsCiudades(this);">
					<c:forEach items="${provincias}" var="provincia">
						<option value="${provincia.id}">${provincia.nombre}</option>
					</c:forEach>
				</select>
				<br>
				<br>
				<select disabled id="selectOptionCiudad" class="selectpicker remove-example" data-live-search="true" title="Ciudad" onchange="habilitarBoton();">
				</select>
				<br>
				<br>
				<button id="buttonFiltro" disabled="disabled" onclick="filtrar();" class="btn btn-success rm-relish pull-right">Aplicar filtro</button>
	


<!-- 				<form> -->
<!-- 					<div class="form-group"> -->
<%-- 						<c:forEach items="${ciudades}" var="ciudad"> --%>
<!-- 							<div class="checkbox"> -->
<!-- 								<label> <a -->
<%-- 									href="categoria-${categoria}-0-12-${ciudad.codPostal}">${ciudad.nombre}</a> --%>
<!-- 								</label> -->
<!-- 							</div> -->
<%-- 						</c:forEach> --%>
<!-- 					</div> -->

<!-- 										<button class="btn btn-default btn-sm btn-primary"> -->
<!-- 											<i class="fa fa-pencil"></i> Aplicar -->
<!-- 										</button> -->

<!-- 				</form> -->

			</div>
		</div>


	</div>

</body>
<!-- Latest compiled and minified JavaScript -->

<script
	src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.4/js/bootstrap-select.min.js"></script>
<script type="text/javascript">

	function setearItemsCiudades(obj) {
		var idProvincia = $("#selectOptionProvincia option:selected").val();
		getCiudades(idProvincia);
	}

	function getCiudades(idProvincia) {
		//var token = $("meta[name='_csrf']").attr("content");
		//var header = $("meta[name='_csrf_header']").attr("content");
		//		var text = document.getElementById("textAreaComentario").value;
		//		var idServicio = $('#buttonEliminarModal').attr("name");
		//var idUrgencia = obj.getAttribute("value");
		//		var idPublicacion = obj;
		//		var datos = {
		//			id : idPublicacion
		//		};
		$.ajax({
			type : "GET",
			//contentType : "application/json",
			url : 'ciudades-' + idProvincia,

			//dataType : 'json',
			//timeout : 100000,
			//			beforeSend : function(request) {
			//				request.setRequestHeader(header, token);
			//			},
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#selectOptionCiudad').find('option').remove().end()
				$.each(datos, function(index) {
					//		            alert(datos[index].nombre);
					$('#selectOptionCiudad').append(
							'<option id='+datos[index].id +'>'
									+ datos[index].nombre + '</option>')
				});
				$('.remove-example').find('[value=Ficticio]').remove();
				$('.remove-example').selectpicker('refresh');
				$('#selectOptionCiudad').prop('disabled', false);
				$('#selectOptionCiudad').selectpicker('refresh');
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
	
	function filtrar(){
		var selectCiudad = document.getElementById("selectOptionCiudad");
		var id = selectCiudad.options[selectCiudad.selectedIndex].id;
		window.location.href = "categoria-"+${categoria}+"-0-12-"+id;
	}
	
	var ciudad = '${ciudad}';
	if ( ciudad != ''){
		getCiudadesAlInicio(${ciudad.provincia.id});
	}
	$('#selectOptionProvincia').selectpicker('val', ${ciudad.provincia.id});
	
	function getCiudadesAlInicio(idProvincia) {
		$.ajax({
			type : "GET",
			url : 'ciudades-' + idProvincia,
			success : function(datos) {
				console.log("SUCCESS: ", datos);
				$('#selectOptionCiudad').find('option').remove().end()
				$.each(datos, function(index) {
					$('#selectOptionCiudad').append(
							'<option id='+datos[index].id +'>'
									+ datos[index].nombre + '</option>')
				});
				$('.remove-example').find('[value=Ficticio]').remove();
				$('.remove-example').selectpicker('refresh');
				$('#selectOptionCiudad').prop('disabled', false);
				$('#selectOptionCiudad').selectpicker('refresh');
				$('#selectOptionCiudad').selectpicker('val', '${ciudad.nombre}');
			},
			error : function(e) {
				console.log("ERROR: ", e);
			},
			done : function(e) {
				console.log("DONE");
			}
		});
	}
	
	function habilitarBoton(){
		$('#buttonFiltro').prop('disabled', false);
	}
	
</script>


</html>