<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="robots" content="all,follow">
	<meta name="googlebot" content="index,follow,snippet,archive">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Obaju e-commerce template">
	<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
	<meta name="keywords" content="">
	
	<title>Gestionar denuncias</title>
	
	<meta name="keywords" content="">
	<link ref='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

	<!-- styles -->
	<link href="css/font-awesome.css" rel="stylesheet">
	<link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/animate.min.css" rel="stylesheet">
	<link href="css/owl.carousel.css" rel="stylesheet">
	<link href="css/owl.theme.css" rel="stylesheet">

	<!-- theme stylesheet -->
	<link href="css/style.default.css" rel="stylesheet" id="theme-stylesheet">

	<!-- your stylesheet with modifications -->
	<link href="css/custom.css" rel="stylesheet">

	<script src="js/respond.min.js"></script>

	<link rel="shortcut icon" href="favicon.png">

	<script type="text/javascript">
		window.onload = function() {
			$("#liDenuncias").attr("class", "active");
		}
	</script>
</head>

<body>
	<%@include file="header.jsp"%>

	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">
					<ul class="breadcrumb">
						<li><a href="index">Home</a></li>
						<li>Gestionar denuncias</li>
					</ul>
				</div>

				<%@include file="menu-lateral.jsp"%>

				<div class="col-md-9" id="customer-orders">
					<div class="box">
						<h1>Denuncias realizadas por usuarios</h1>

						<!--                         <p class="lead">Seleccione los servicios urgentes a los cuales puede atender inmediatamente.</p> -->
						<p class="text-muted">�sta es la lista de todos las denuncias realizadas por los usuarios</p>

						<hr>

						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Fecha y hora</th>
										<th>Texto</th>
										<th>Publicaci�n</th>
										<th>Usuario denunciado</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${listaDenuncias}" var="denuncia">
										<tr>
											<td>${denuncia.fechaHora}</td>
										
											<td>${denuncia.texto}</td>
											
											<th>
												<a href="detail-publicacion-${denuncia.publicacion.id}" title= "Ver publicaci�n">
													${denuncia.publicacion.titulo}
												<a>
											</th>
										
											<th>
												<a href="bloqdesbloquser-${denuncia.publicacion.especialista.user.id}" title= "Bloquear Usuario">
													${denuncia.publicacion.especialista.user.username}
												<a>											
											</th>
										</tr>
									</c:forEach>
								</tbody>
							</table>
						</div>
					</div>
				</div>

			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<!-- *** FOOTER ***____________________ -->
		<%@include file="footer.jsp"%>

		<div class="modal fade" id="confirm-delete" tabindex="-1"
			role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">Eliminar urgencia</div>
					<div class="modal-body">�Est� seguro que desea eliminar la
						solicitud de urgencia?</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
						<a href="#" id="buttonEliminarModal" class="btn btn-danger btn-ok">Eliminar</a>
					</div>
				</div>
			</div>
		</div>
</body>

<script type="text/javascript">
	function setValorBotonEliminarModal(obj) {
		$("#buttonEliminarModal").attr("href", obj.getAttribute("value"));
		return false;
	}
</script>

<script type="text/javascript">
	function cambiarIconoaEliminar(obj) {
		$("#icon" + obj.getAttribute("id")).attr("class", "fa fa-times");
		//$("#"+obj.getAttribute("id")).attr("class", "btn btn-primary btn-sm")
	}
	function cambiarIconoaReloj(obj) {
		$("#icon" + obj.getAttribute("id")).attr("class", "fa fa-clock-o");
		//$("#"+obj.getAttribute("id")).attr("class", "btn btn-primary btn-sm")
	}
</script>
</html>

