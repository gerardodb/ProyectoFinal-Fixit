<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="ISO-8859-1">
<meta name="robots" content="all,follow">
<meta name="googlebot" content="index,follow,snippet,archive">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Obaju e-commerce template">
<meta name="author" content="Ondrej Svestka | ondrejsvestka.cz">
<meta name="keywords" content="">

<title>Obaju : e-commerce template</title>

<meta name="keywords" content="">

<link
	href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100'
	rel='stylesheet' type='text/css'>

<!-- styles -->
<link href="css/font-awesome.css" rel="stylesheet">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/animate.min.css" rel="stylesheet">
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">

<!-- theme stylesheet -->
<link href="css/style.default.css" rel="stylesheet"
	id="theme-stylesheet">

<!-- your stylesheet with modifications -->
<link href="css/custom.css" rel="stylesheet">

<script src="js/respond.min.js"></script>

<link rel="shortcut icon" href="favicon.png">

<script type="text/javascript">
	window.onload = function() {
		$("#liCategorias").attr("class", "active");
	}
</script>

</head>

<body>
	<%@include file="header.jsp"%>
	<div id="all">

		<div id="content">
			<div class="container">

				<div class="col-md-12">

					<ul class="breadcrumb">
						<li><a href="#">Home</a></li>
						<li>Agregar Categor�a</li>
					</ul>

				</div>

				<div class="col-md-6">
					<div class="box">

						<h1>Cargar categor�a</h1>
						<p class="text-muted"></p>
						<hr>

						<form:form modelAttribute="categoria" method="POST"
							acceptCharset="ISO-8859-1">
							<form:input type="hidden" path="id" id="id" />
							<div class="form-group">
								<label for="name">Nombre de Categoria</label>
								<form:input maxlength="30" type="text" path="nombre" id="nombre"
									class="form-control" />
								<form:errors path="nombre" cssClass="error" />
							</div>

							<div class="form-group">
								<label for="state">Categor�a Padre</label>
								<form:select class="form-control" itemValue="id" path="padre.id"
									items="${itemsPadres}" itemLabel="nombre" id="drownmenuPadre">
								</form:select>
							</div>

							<div class="form-group">
								<input id="checkboxPadre" type="checkbox" onchange="activarDesactivarDrownmenu();">
								<label>Es una categoria padre</label>
							</div>

							<div class="text-center">
								<button type="submit" class="btn btn-primary">
									<i class="fa fa-user-md"></i> Guardar
								</button>
							</div>
						</form:form>
					</div>
				</div>
			</div>
			<!-- /.container -->
		</div>
		<!-- /#content -->


		<%@include file="footer.jsp"%>
</body>

<script type="text/javascript">
	if (${categoria.padre.id}==0){
		$("#drownmenuPadre").prop("disabled", true);
		$('#checkboxPadre').prop('checked', true);
	}
	

function activarDesactivarDrownmenu(){
	if ($('#checkboxPadre').is(':checked')){
		$("#drownmenuPadre").prop("disabled", true);
	}else{
		$("#drownmenuPadre").prop("disabled", false);
	}
}
</script>

</html>
