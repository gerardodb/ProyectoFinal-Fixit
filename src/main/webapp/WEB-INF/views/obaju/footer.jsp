<!DOCTYPE html>
<html lang="en">

<head>
</head>

<body>

	<!-- *** FOOTER ***
 _________________________________________________________ -->
	<div id="footer" data-animate="fadeInUp">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-6">
<!-- 					<h4>Paginas</h4> -->

<!-- 					<ul> -->
<!--  						<li><a href="text.html">Sobre nosotros</a></li>  -->
<!--  						<li><a href="text.html">Terminos y condiciones</a></li> -->
<!--  						<li><a href="faq.html">FAQ</a></li> -->
<!-- 						<li><a href="contact.html">Cont�ctanos</a></li> -->
<!-- 					</ul> -->

<!-- 					<hr> -->

					<h4>Secci�n de usuario</h4>

					<ul>
						<li><a href="#" data-toggle="modal"
							data-target="#login-modal">Inicio de Sesi�n</a></li>
						<li><a href="register.html">Registro</a></li>
					</ul>

					<hr class="hidden-md hidden-lg hidden-sm">

				</div>
				<!-- /.col-md-3 -->

<!-- 				<div class="col-md-4 col-sm-6"> -->

<!-- 					<h4>Categorias</h4> -->

<!-- 					<h5>Profesores</h5> -->

<!-- 					<ul> -->
<!-- 						<li><a href="category.html">Ingles</a></li> -->
<!-- 						<li><a href="category.html">Matematica</a></li> -->
<!-- 					</ul> -->

<!-- 					<h5>Domiciliario</h5> -->
<!-- 					<ul> -->
<!-- 						<li><a href="category.html">Electricista</a></li> -->
<!-- 						<li><a href="category.html">Plomero</a></li> -->
<!-- 					</ul> -->

<!-- 					<hr class="hidden-md hidden-lg"> -->

<!-- 				</div> -->
				<!-- /.col-md-3 -->

				<div class="col-md-4 col-sm-6">

					<h4>Donde encontrarnos</h4>

					<p>
						<strong>Fixit S.A.</strong><br>French 464 <br>Resistencia, Chaco<br> <strong>Argentina</strong>
					</p>

<!-- 					<a href="contact.html">Ir a pagina de contacto</a> -->

					<hr class="hidden-md hidden-lg">

				</div>
				<!-- /.col-md-3 -->



				<div class="col-md-4 col-sm-6">

<!-- 					<h4>Get the news</h4> -->

<!-- 					<p class="text-muted">Pellentesque habitant morbi tristique -->
<!-- 						senectus et netus et malesuada fames ac turpis egestas.</p> -->

<!-- 					<form> -->
<!-- 						<div class="input-group"> -->

<!-- 							<input type="text" class="form-control"> <span -->
<!-- 								class="input-group-btn"> -->

<!-- 								<button class="btn btn-default" type="button">Subscribe!</button> -->

<!-- 							</span> -->

<!-- 						</div> -->
<!-- 						/input-group -->
<!-- 					</form> -->

<!-- 					<hr> -->

					<h4>Siguenos!</h4>

					<p class="social">
						<a href="https://www.facebook.com/FixitArg/" class="facebook external" data-animate-hover="shake"><i
							class="fa fa-facebook"></i></a> <a href="https://twitter.com/fixitpak?lang=es" class="twitter external"
							data-animate-hover="shake"><i class="fa fa-twitter"></i></a> <a
							href="https://www.instagram.com/explore/tags/fixit/" class="instagram external" data-animate-hover="shake"><i
							class="fa fa-instagram"></i></a> <a href="https://plus.google.com/u/0/+ifixit" class="gplus external"
							data-animate-hover="shake"><i class="fa fa-google-plus"></i></a>
<!-- 						<a href="#" class="email external" data-animate-hover="shake"><i -->
<!-- 							class="fa fa-envelope"></i></a> -->
					</p>


				</div>
				<!-- /.col-md-3 -->

			</div>
			<!-- /.row -->

		</div>
		<!-- /.container -->
	</div>
	<!-- /#footer -->

	<!-- *** FOOTER END *** -->




	<!-- *** COPYRIGHT ***
 _________________________________________________________ -->
	<div id="copyright">
		<div class="container">
			<div class="col-md-6">
				<p class="pull-left">� 2017 Fixit S.A.</p>

			</div>
			<div class="col-md-6">
				<p class="pull-right">
					Template by <a
						href="https://bootstrapious.com/e-commerce-templates">Bootstrapious</a>
					& <a href="https://fity.cz">Fity</a>
					<!-- Not removing these links is part of the license conditions of the template. Thanks for understanding :) If you want to use the template without the attribution links, you can do so after supporting further themes development at https://bootstrapious.com/donate  -->
				</p>
			</div>
		</div>
	</div>
	<!-- *** COPYRIGHT END *** -->



	</div>
	<!-- /#all -->




	<!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
	
	

</body>
</html>